from Case import Case

class TableauCases:

    def __init__(self, nombreColonne, nombreLigne, prctgForet):
        self.ColonneMax = nombreColonne
        self.LigneMax = nombreLigne
        self.ColonneActuelle = 0
        self.LigneActuelle = 0
        self.TableauCase = []
        self.TableauRempli = False
        self.InitTableauCase()
        self.TableauChang = []
        self.InitTableauChang()
        for Compteur1 in range(0,self.LigneMax):
            for Compteur2 in range(0,self.ColonneMax):
                CaseTemp = Case(prctgForet)
                if self.TableauRempli == False :
                    self.AjoutCase(CaseTemp)

    def InitTableauChang(self):
        for Compteur1 in range(self.LigneMax):
            self.TableauChang.append([0] * self.ColonneMax)

    def RAZTableauChang(self):
        for Compteur1 in range(self.LigneMax):
            for Compteur2 in range(self.ColonneMax):
                self.TableauChang[Compteur1][Compteur2] = 0

    def InitTableauCase(self):
        for Compteur1 in range(self.LigneMax):
            CaseTemp = Case(0)
            self.TableauCase.append([CaseTemp] * self.ColonneMax)

    def AjoutCase(self, Case = Case):
        if self.TableauRempli == False :
            self.TableauCase[self.LigneActuelle][self.ColonneActuelle] = Case
            Case.setPosition(self.LigneActuelle,self.ColonneActuelle)
            self.ColonneActuelle+= 1
            if self.ColonneActuelle == self.ColonneMax :
                self.ColonneActuelle = 0
                self.LigneActuelle += 1
                if self.LigneActuelle == self.LigneMax :
                    self.TableauRempli = True
    
    def AffichageTableauCase(self):
        for Compteur1 in self.TableauCase :
            for Compteur2 in Compteur1 :
                print(Compteur2.getEtat())
            print("\n")

    def getTableauRempli(self):
        return self.TableauRempli

    def getTableauCase(self):
        return self.TableauCase

    def getColonneMax(self):
        return self.ColonneMax

    def getLigneMax(self):
        return self.LigneMax
    
    def getColonneActuelle(self):
        return self.ColonneActuelle

    def getLigneActuelle(self):
        return self.LigneActuelle

    def getCaseOfTableau(self, coordonneeX, coordonneeY):
        return self.TableauCase[coordonneeX][coordonneeY]

    def ChgmtCase(self):
        self.RAZTableauChang()
        for Compteur1 in range(0,self.getLigneMax()) :
            for Compteur2 in range(0,self.getColonneMax()) :
                Case1 = self.TableauCase[Compteur1][Compteur2]
                if(Case1.getEtat()=="Vide"):
                    self.TableauCase[Compteur1][Compteur2].chgmtCaseVide()
                    self.TableauChang[Compteur1][Compteur2]=1
                if(Case1.getEtat()=="Cendre"):
                    self.TableauCase[Compteur1][Compteur2].chgmtCaseCendreToVide()
                    self.TableauChang[Compteur1][Compteur2]=1
                if(Case1.getEtat()=="Feu"):
                    Case1.chgmtCaseFeuToCendre()
                    self.chgmtCaseArbreToFeu(Case1)
                    self.TableauChang[Compteur1][Compteur2]=1
                if(Case1.getEtat()=="Forêt") and (Case1.getFuturEtat()!="Feu"):
                    Case1.FuturEtat = "Forêt"
        self.chgmtEtat()

    def chgmtEtat(self):
        for Compteur1 in range (0,self.getLigneMax()):
            for Compteur2 in range (0,self.getColonneMax()):
                self.TableauCase[Compteur1][Compteur2].chgmtEtatToFuturEtat()


    def chgmtCaseArbreToFeu(self, Case = Case):
        Position = Case.getPosition()
        PositionX = Position[0]
        PositionY = Position[1]
        

        if (PositionX > 0) :
            if(self.TableauCase[PositionX-1][PositionY].getEtat()=="Forêt"):
                self.TableauCase[PositionX-1][PositionY].setFuturEtat("Feu")
                self.TableauChang[PositionX-1][PositionY] = 1

        if (PositionY < self.getColonneMax()-1) :
            if(self.TableauCase[PositionX][PositionY+1].getEtat()=="Forêt"):
                self.TableauCase[PositionX][PositionY+1].setFuturEtat("Feu")
                self.TableauChang[PositionX][PositionY+1] = 1
                
        if (PositionX < self.getLigneMax()-1) :
            if(self.TableauCase[PositionX+1][PositionY].getEtat()=="Forêt"):
                self.TableauCase[PositionX+1][PositionY].setFuturEtat("Feu")
                self.TableauChang[PositionX+1][PositionY] = 1

        if (PositionY > 0) :
            if(self.TableauCase[PositionX][PositionY-1].getEtat()=="Forêt"):
                self.TableauCase[PositionX][PositionY-1].setFuturEtat("Feu")
                self.TableauChang[PositionX][PositionY-1] = 1
