from tkinter import Tk, Canvas, Label, Button
from Tableau_Case import TableauCases
import argparse
import sys
import time

def etat_to_color(etat):
    if etat == 'Forêt':
        return 'green'
    elif etat == 'Feu':
        return 'red'
    elif etat == 'Cendre':
        return 'gray'
    elif etat == 'Vide':
        return 'white'

def click_simStart():
    if Tableau.TableauRempli == True:
        Tableau.TableauRempli = False
        Tableau.ChgmtCase()
        for compteur1 in range(nombreLignes):
            for compteur2 in range(nombreColonne):
                if Tableau.TableauChang[compteur1][compteur2] == 1 :
                    canvas.create_rectangle(compteur2 * tailleCase, 
                        compteur1 * tailleCase, (compteur2 + 1) * tailleCase,
                        (compteur1 + 1) * tailleCase, fill = 
                        etat_to_color(Tableau.TableauCase[compteur1][compteur2].getEtat()))
                    Tableau.TableauRempli = True
        frame.after(2000, click_simStart)
    else:
        Tableau.TableauRempli = True

def click_callback(event):
    colonne = (event.x // tailleCase) * tailleCase
    lig = (event.y // tailleCase) * tailleCase
    color = 'white'
    if event.num == 1:
        color = 'red'
        Tableau.TableauCase[event.y // tailleCase][event.x // tailleCase].Etat = 'Feu'
    elif event.num == 2:
        color = 'white'
        Tableau.TableauCase[event.y // tailleCase][event.x // tailleCase].Etat = 'Vide'
    elif event.num == 3:
        color = 'green'
        Tableau.TableauCase[event.y // tailleCase][event.x // tailleCase].Etat = 'Forêt'
    canvas.create_rectangle(colonne, lig, colonne + tailleCase, lig + tailleCase, fill=color)

parser = argparse.ArgumentParser(
    description="Vous devez passer les arguements suivants :")
parser.add_argument("<nombreRow>", type=int, help="Le nombre de lignes attendues")
parser.add_argument("<nombreColumns>", type=int, 
    help="Le nombre de colonnes attendues")
parser.add_argument("<Case_size>", type=int, 
    help="La taille en pixel des cases")
parser.add_argument("<afforestation>", type=float, 
    help="Probabilité (<= 1) que la case soit boisée (ex : 0.6)")
args = parser.parse_args()

if float(sys.argv[4]) > 1 :
    print('<afforestation> doit être inférieur ou égal à 1 !')
    exit(1)
nombreLignes = int(sys.argv[1])
nombreColonne = int(sys.argv[2])
tailleCase = int(sys.argv[3])
afforestation = float(sys.argv[4])

Tableau = TableauCases(nombreColonne, nombreLignes, afforestation)

frame = Tk()
frame.title("Simulateur de feux de forêt")
canvas = Canvas(frame, width = nombreLignes * tailleCase, 
    height = nombreColonne * tailleCase)

canvas.bind('<Button-1>', click_callback)
canvas.bind('<Button-2>', click_callback)
canvas.bind('<Button-3>', click_callback)
canvas.pack()
canvas.grid(column=0, row=0, rowspan=2)

for compt in range(nombreLignes):
    for compteur2 in range(nombreColonne):
        color = etat_to_color(Tableau.TableauCase[compt][compteur2].getEtat())
        canvas.create_rectangle(compteur2 * tailleCase, compt * tailleCase, 
                (compteur2 + 1) * tailleCase, (compt + 1) * tailleCase, 
                fill = color)

lbl_explications = Label(frame, 
    text='<ClickGauche> pour mettre le feu à une case \n'
        + '<ClickMolette> pour retirer un arbre \n'
        + '<ClickDroit> pour mettre un arbre sur une case')
lbl_explications.grid(column=1, row=0)

btn_simStart = Button(frame, text = 'START', command = click_simStart)
btn_simStart.grid(column=1, row=1)

frame.mainloop()
